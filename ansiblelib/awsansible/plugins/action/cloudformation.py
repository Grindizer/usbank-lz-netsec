# coding=utf-8
# Make coding more python3-ish

import sys
from functools import wraps

from awsansible.lib import print_stack_status

try:
    from __main__ import display
except ImportError:
    from ansible.utils.display import Display

    display = Display()

from ansible.plugins.action import ActionBase
from ansible.errors import AnsibleActionFail
from multiprocessing.pool import ThreadPool
from ansible.module_utils.parsing.convert_bool import boolean
import time


def ensure_templated(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        self = args[0]
        res = f(*args, **kwargs)
        return self._templar.template(res)

    return wrapper


class ActionModule(ActionBase):
    @ensure_templated
    def _compute_cf_tags(self, task_vars):
        stack_tags = task_vars.get('stack_tags', {})
        info_tags = task_vars.get('info_tags', {})
        task_tags = self._task.args.get('tags', {})
        return {**info_tags, **task_tags, **stack_tags}

    @ensure_templated
    def _compute_cf_region(self, task_vars):
        global_region = task_vars.get('region', {})
        task_region = self._task.args.get('region', {})
        return task_region if task_region else global_region

    @ensure_templated
    def _compute_cf_stack_name(self, task_vars):
        global_stack_name = task_vars.get('stack_name', None)
        task_stack_name = self._task.args.get('stack_name', None)
        return task_stack_name if task_stack_name else global_stack_name

    def _compute_cf_template(self, task_vars):
        template_tpl = self._task.args.get('template_tpl', None)
        outputs = task_vars.get('output_dir', None)
        inventory_hostname_short = task_vars.get('inventory_hostname_short', None)
        stack_name = self._compute_cf_stack_name(task_vars)
        if template_tpl:
            del self._task.args['template_tpl']
            args = {'src': template_tpl,
                    'dest': self._templar.template(f'{outputs}/{inventory_hostname_short}-{stack_name}.yml'),
                    'force': 'yes'}

            tpl_task = self._task.copy()
            tpl_task.action = 'template'
            tpl_task.args = args
            tpl_action = self._shared_loader_obj.action_loader.get('template', task=tpl_task,
                                                                   connection=self._connection,
                                                                   play_context=self._play_context, loader=self._loader,
                                                                   templar=self._templar,
                                                                   shared_loader_obj=self._shared_loader_obj)
            tpl_result = tpl_action.run(tmp=None, task_vars=task_vars)
            if 'failed' in tpl_result:
                raise AnsibleActionFail(result=tpl_result, message=tpl_result['msg'])
            d, p = tpl_result.get('dest', None), tpl_result.get('path', None)
            return d if d else p
        else:
            return self._task.args.get('template', None)

    def _execute_module_async(self, module_name, module_args, tmp, task_vars, poll=1):
        pool = ThreadPool(processes=2)
        async_task = pool.apply_async(self._execute_module, (module_name, module_args, tmp, task_vars))

        while not async_task.ready():
            time.sleep(poll)
            facts = self._stack_facts(task_vars)
            if facts: print_stack_status(facts)
        sys.stdout.write('\n')
        return async_task.get()

    def _stack_facts(self, task_vars, events=True, template=True, resources=True):
        name, region = self._task.args['stack_name'], self._task.args['region']
        args = {'region': region, 'stack_name': name, 'stack_events': events, 'stack_template': template,
                'stack_resources': resources}
        for _ in range(3):
            results = self._execute_module(module_name="cloudformation_facts", module_args=args, tmp=None,
                                           task_vars=task_vars)
            if 'failed' not in results:
                facts = results['ansible_facts']['cloudformation']
                return facts.get(name, None)
        return None

    def run(self, tmp=None, task_vars=None):
        """ handler for file transfer operations """
        display.v("Yaac-ify cloudformation module")
        result = super(ActionModule, self).run(tmp, task_vars)

        self._task.args['tags'] = self._compute_cf_tags(task_vars)
        self._task.args['stack_name'] = self._compute_cf_stack_name(task_vars)
        self._task.args['region'] = self._compute_cf_region(task_vars)
        self._task.args['template'] = self._compute_cf_template(task_vars)

        # force disable rollback ... that's annoying when testing.
        self._task.args['disable_rollback'] = True

        # How about progress?
        show_progress = boolean(self._task.args.get('show_progress', 'yes'))
        if 'show_progress' in self._task.args:
            del self._task.args['show_progress']

        _execute = self._execute_module
        if show_progress:
            _execute = self._execute_module_async

        cf_results = _execute(module_name='cloudformation', module_args=self._task.args, tmp=tmp, task_vars=task_vars)

        result.update(cf_results)
        return result
