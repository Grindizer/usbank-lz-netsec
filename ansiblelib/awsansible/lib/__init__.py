import os
import sys
import fcntl
from struct import unpack, pack
from termios import TIOCGWINSZ
from . cfutils import stack_status_summary, render_stack_status, filter_stacks_by_tags, cf2objs


def print_stack_status(facts):
    if os.isatty(0):
        tty_size = unpack('HHHH', fcntl.ioctl(0, TIOCGWINSZ, pack('HHHH', 0, 0, 0, 0)))[1]
    else:
        tty_size = 0
    cols = max(79, tty_size - 1)

    progress = render_stack_status(pg=stack_status_summary(facts), cols=cols)
    sys.stdout.write('\r' + progress)
    sys.stdout.flush()
