from operator import itemgetter
from collections import Counter, namedtuple
from ansible.utils.display import stringc
from awscli.customizations.cloudformation.yamlhelper import yaml_parse
import yaml
import funcy as fn

StackStatusSummary = namedtuple('StackStatusSummary', 'name status total complete progress failed message')

stack_template = itemgetter('stack_template')
stack_status = lambda f: f['stack_description']['stack_status']
stack_name = lambda f: f['stack_description']['stack_name']
stack_resources = itemgetter('stack_resource_list')
stack_events = itemgetter('stack_events')
resource_id = itemgetter('LogicalResourceId')
resource_status = itemgetter('ResourceStatus')
resource_status_reason = itemgetter('ResourceStatusReason')
status_action = lambda s: s.split('_')[0]
status_result = lambda s: s.split('_')[-1]


def stack_resource_count(template):
    try:
        tmpl = yaml_parse(template)
        total = len(tmpl['Resources'])
    except yaml.parser.ParserError:
        total = None

    return total


def stack_status_summary(facts):
    """
    >>> facts = {'stack_template': 'Resources:\\n  - res01\\n  - res02\\n  - res03\\n  - res04\\n  - res05\\n  - res06'}
    >>> facts['stack_description'] = {'stack_status': 'create_in_progress', 'stack_name': 'a long name'}
    >>> facts['stack_resource_list'] = [{'ResourceStatus': 'COMPLETE'}, {'ResourceStatus': 'COMPLETE'},
    ... {'ResourceStatus': 'FAILED'}, {'ResourceStatus': 'PROGRESS'}]
    >>> facts['stack_events'] = [{'LogicalResourceId': 'resourceid', 'ResourceStatus': 'CREATE_IN_PROGRESS'}]
    >>> p = stack_status_summary(facts)
    >>> p.name
    'a long name'
    >>> p.total
    6
    >>> p.complete
    2
    >>> p.message
    'CREATE_IN_PROGRESS resourceid'
    """
    total = stack_resource_count(stack_template(facts))
    status = stack_status(facts)
    name = stack_name(facts)
    resource_list = stack_resources(facts)
    rcount = Counter([status_result(resource_status(res)) for res in resource_list])
    events = stack_events(facts)

    message = f'{status:>15} {name} '
    if status_result(status) != 'FAILED':
        latest_event = events[0]
        message = f'{resource_status(latest_event):>15} {resource_id(latest_event)}'
    else:
        for res in resource_list:
            if status_result(resource_status(r)) == 'FAILED' and 'cancelled' not in resource_status_reason(r):
                message = f'{resource_id(res):>15} {resource_status_reason(res)}'

    return StackStatusSummary(name, status, total, rcount['COMPLETE'], rcount['PROGRESS'], rcount['FAILED'], message)


def render_stack_status(pg, cols):
    """
    >>> pg = StackStatusSummary('a long name', 'CREATE', total=6, complete=2, progress=1, failed=1,
    ... message='resourceid create in progress')
    >>> render_stack_status(pg, 79), len(render_stack_status(pg, 79))
    ('    a long name [============......] 33% [ resourceid create in progress  ]    ', 79)
    """
    colors = ['green', 'yellow', 'red']
    cursor = 'x' if pg.status[:6] == 'DELETE' else '='
    counts = pg.complete, pg.progress, pg.failed
    rcols = int(round(cols * 0.90))
    pad = (cols - rcols) / 2
    name_len = min(len(pg.name), 15)
    bar_len = int(round(0.25 * rcols))
    message_len = rcols - (name_len + bar_len + 12)

    fills = [int(round(bar_len * count / float(pg.total))) for count in counts]
    percents = int(round(100 * counts[0] / float(pg.total)))
    bar = ''.join(stringc(cursor * fill, color) for fill, color in zip(fills, colors))
    bar = bar + ('.' * (bar_len - sum(fills)))
    return f'{" ":^{pad}}{pg.name[:name_len]:<{name_len}} ' \
           f'[{bar}] ' \
           f'{percents:2}% ' \
           f'[ {pg.message:<{message_len}} ]{" ":^{pad}}'


@fn.autocurry
def map2obj(aws_objs, key1, key2):
    return [(t[key1], t[key2]) for t in aws_objs]


cf2objs = {'outputs': map2obj(key1='OutputKey', key2='OutputValue'),
           'parameters': map2obj(key1='ParameterKey', key2='ParameterValue'),
           'tags': map2obj(key1='Key', key2='Value')}


def filter_stacks_by_tags(stacks, **tags):
    map2tags = cf2objs.get('tags')
    return [stack for stack in stacks if tags.items() <= set(map2tags(stack.tags))]
